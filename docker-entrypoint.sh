#!/bin/bash

set -e

if [ -z "$1" ]; then
  java -jar app.jar
fi

exec "$@"